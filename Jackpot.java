import java.util.Scanner;
public class Jackpot {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		boolean playing = true;
		char playAgain = 'y';
		while(playing) {
			playGame();
			System.out.println("\n" + "Would you like to play again? (y or n)");
			playAgain = reader.next().charAt(0);
			if(playAgain == 'y') {
			}
			else {
				playing = false;
				System.out.println("\n" + "Thank you for playing!");
			}
		}	
	}
	
	public static void playGame() {
		System.out.println("\n" + "Welcome to the game of Jackpot!");
		Board board = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		while(!gameOver) {
			System.out.println(board);
			if(board.playATurn()) {
				gameOver = true;
			}
			else {
				numOfTilesClosed += 1;
			}
		}
		
		if(numOfTilesClosed >= 7) {
			System.out.println("You have reached the jackpot! Congratulations, you win!");
		}
		else {
			System.out.println("You have flipped less than 7 tiles. You lost.");
		}
	}
}