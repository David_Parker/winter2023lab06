import java.util.Random;
public class Die {
	//fields
	private int faceValue;
	private Random roll;
	
	//constructor
	public Die() {
		this.faceValue = 1;
		Random roll = new Random(); 
		this.roll = roll;
	}
	
	public int getFaceValue() {
		return this.faceValue;
	}
	
	public void roll() {
		this.faceValue = this.roll.nextInt(6)+1;
	}
	
	public String toString() {
		return "Face value: " + this.faceValue;
	}
}