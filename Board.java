public class Board {
	//fields
	private Die die1;
	private Die die2;
	private boolean[] isShut;
	
	//constructor
	public Board() {
		this.die1 = new Die();
		this.die2 = new Die();
		this.isShut = new boolean[12];
	}
	
	//toString override
	public String toString() {
		String emptyTiles = "";
		for(int i=0; i<this.isShut.length; i++) {
			if(!isShut[i]) {
				emptyTiles += (i+1) + spacer(i);
			}
			else {
				emptyTiles += "X" + spacer(i);
			}
		}
		return emptyTiles;
	}
	
	//instance methods
	public boolean playATurn() {
		this.die1.roll();
		System.out.println("Die 1 value: " + this.die1.getFaceValue());
		this.die2.roll();
		System.out.println("Die 2 value: " + this.die2.getFaceValue());
		
		int sumOfDice = this.die1.getFaceValue() + this.die2.getFaceValue();
		if(!isShut[sumOfDice-1]) {
			isShut[sumOfDice-1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice + "\n");
			return false;
		}
		else if (!isShut[this.die1.getFaceValue()-1]) {
			isShut[this.die1.getFaceValue()-1] = true;
			System.out.println("Closing tile with the same value as die one: " + this.die1.getFaceValue() + "\n");
			return false;
		}
		else if (!isShut[this.die2.getFaceValue()-1]) {
			isShut[this.die2.getFaceValue()-1] = true;
			System.out.println("Closing tile with the same value as die two: " + this.die2.getFaceValue() + "\n");
			return false;
		}
		else {
			System.out.println("All the tiles for these values are already shut");
			return true;
		}
	}
	
	
	//helper method used to print out the numbers cleanly
	public String spacer(int position) {
		if(position == 11) {
			return "";
		}
		else {
			return " ";
		}
	}
}

